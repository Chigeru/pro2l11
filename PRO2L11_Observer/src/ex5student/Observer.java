package ex5student;

public interface Observer {
    public void update(String color);

}
