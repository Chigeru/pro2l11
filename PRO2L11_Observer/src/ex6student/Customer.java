package ex6student;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private String name;
    private List<Book> Books = new ArrayList<Book>();

    public Customer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    public void addBook(Book b) {
        Books.add(b);
        b.addCustomer(this);
    }

    public void removeBook(Book b) {
        Books.remove(b);
        b.addCustomer(this);
    }
}
