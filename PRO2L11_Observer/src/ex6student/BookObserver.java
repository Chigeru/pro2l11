package ex6student;

public interface BookObserver {
    private void update(Book book);
}
