package ex6student;

import java.util.ArrayList;
import java.util.List;

public class Book implements BookObserver {
	private String title;
	private int count;
	private List<Customer> customer = new ArrayList<Customer>();

	public Book(String title) {
		this.title = title;
		this.count = 0;
	}

	public String getTitle() {
		return title;
	}

	public int getCount() {
		return count;
	}

	public void incCount(int amount) {
		count += amount;
	}

	public void decCount(int amount) {
		count -= amount;
		notifyObservers();
	}

	@Override
	public String toString() {
		return title + "(" + count + ")";
	}

	public void addCustomer(Customer c) {
		customer.add(c);
		c.addBook(this);
	}

	public void removeCustomer(Customer c) {
		customer.remove(c);
		c.addBook(this);
	}

	public void notifyObservers() {
		for (Customer bo : customer) {
			bo.update(this.count);
		}
	}

	@Override
	public void update(Book book) {

	}
}
